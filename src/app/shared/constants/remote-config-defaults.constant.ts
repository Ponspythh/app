export const REMOTE_CONFIG_DEFAULTS = {
  ff_buyCryptoHomeWalletButton: false,
  ff_buyCriptoHomeCard: false,
  ff_buyCryptoNewInvestmentFooter: false,
  ff_buyCryptoAddAmountInvestmentFooter: false,
  ff_updateAppModal: false,
  referralsMenuUrl_referralPromotionCard: 'referrals/closed',
  ff_swap: false,
  ff_homeWalletBackupWarningModal: false,
  ff_educationCardAvailable: false,
  investmentProducts: undefined,
  onOffRampsProviders: undefined,
  ff_newLogin: false,
};
