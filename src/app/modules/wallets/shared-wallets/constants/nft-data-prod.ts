import nftAbi from './nft-abi/nft_abi.json';
import { environment } from 'src/environments/environment';
export const NFT_DATA_PROD = [
  {
    contractAddress: '0x042841842502d3eaf1946f52e77cc5c48f40dff6',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0xd724A626EFEA48745AA5D645170D6D91b23e366e',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0xc724197938eA45efBdb1C9c8b6867E0114979e3c',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0x5892CaE5Ad5f3F4450d031Fa8AFd6e352E2508E7',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0x213Dfe70AE9Afb23e0e98C0479560373042dEB0b',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0x0Ed470Eb897C9cce1e08d4f45098aB15DF903E4C',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0x6b0aCd60E879622E826cBfd0CFFa805204bD5B97',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0xAe30e8a4eD459aBC19D35F08BBcc560a7b02f165',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0xE295856531167865c25932b3abfA7aB9C00f9602',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
  {
    contractAddress: '0x94ad825155b13f6BA2C652F7fEE006DfC87b6227',
    rpc: environment.maticApiUrl,
    abi: nftAbi,
  },
];
