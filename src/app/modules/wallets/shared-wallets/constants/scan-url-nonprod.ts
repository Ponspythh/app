export const NONPROD_SCAN_URLS = {
  MATIC: 'https://mumbai.polygonscan.com/',
  ERC20: 'https://kovan.etherscan.io/',
  BSC_BEP20: 'https://testnet.bscscan.com/',
  RSK: 'https://explorer.testnet.rsk.co/',
};
