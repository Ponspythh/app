import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiUsuariosService } from '../shared-users/services/api-usuarios/api-usuarios.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resend-verification-email',
  template: `
    <ion-content class="ion-padding" *ngIf="this.email">
      <div class="main">
        <div class="main__close_button">
          <ion-button fill="clear" appTrackClick name="Close Resend Email" (click)="this.close()">
            <ion-icon class="main__close_button__icon" name="ux-close" color="neutral80"></ion-icon>
          </ion-button>
        </div>
        <div class="main__ux_success_image">
          <app-ux-center-img imagePath="./assets/img/users/resend-verification-email/success-resend.svg"></app-ux-center-img>
        </div>
        <div class="main__primary_text">
          <ion-text class="ux-font-text-xl">{{
            'users.register.resend_verification_email.title' | translate
          }}</ion-text>
        </div>
        <div class="main__secondary_text">
          <ion-text class="ux-font-text-xs">{{
            'users.register.resend_verification_email.text' | translate
          }}</ion-text>
        </div>
        <div class="main__actions">
          <div class="main__actions__primary">
            <ion-button
              color="secondary"
              class="ux_button"
              appTrackClick
              name="Resend Verification Email"
              [disabled]="this.disableResendEmail"
              (click)="this.resendEmail()"
            >
              {{
                'users.register.resend_verification_email.resend_mail_button' | translate: { timer: this.timerText }
              }}
            </ion-button>
          </div>
          <div class="main__actions__secondary">
            <ion-button class="ux-link-xl" appTrackClick fill="clear" name="Open Ticket" (click)="this.openTicket()">
              {{ 'users.register.resend_verification_email.open_ticket_button' | translate }}
            </ion-button>
          </div>
        </div>
      </div>
    </ion-content>
  `,
  styleUrls: ['./resend-verification-email.page.scss'],
})
export class ResendVerificationEmailPage implements OnInit {
  disableResendEmail = true;
  timerText = '';
  timerSeconds: number;
  private timer: any;

  email: string;

  constructor(
    private apiUsuariosService: ApiUsuariosService,
    private navController: NavController,
    private storage: Storage,
    private router: Router
  ) {}

  ngOnInit() {
    this.email = this.router.getCurrentNavigation().extras?.state?.email;
  }

  ionViewWillEnter() {
    if (this.email) {
      this.updateStorage();
      this.resendEmail();
    } else {
      this.checkStorage();
    }
  }

  async checkStorage() {
    this.storage.get('email').then(async (email) => {
      if (email) {
        this.email = email;
        this.startTimer();
      } else {
        this.close();
      }
    });
  }

  updateStorage() {
    this.updateEmailStorage();
  }

  updateEmailStorage() {
    this.storage.set('email', this.email);
  }

  async startTimer() {
    this.timerSeconds = 60;
    this.timerText = `(${this.timerSeconds}s)`;
    this.disableResendEmail = true;
    this.timer = setInterval(this.decreaseTimer.bind(this), 1000);
  }

  decreaseTimer() {
    this.timerSeconds--;
    this.timerText = `(${this.timerSeconds}s)`;

    if (this.timerSeconds === 0) {
      this.timerText = '';
      clearInterval(this.timer);
      this.disableResendEmail = false;
    }
  }

  close() {
    this.clearStorage();
    this.navController.navigateBack(['/users/login']);
  }

  async resendEmail() {
    this.disableResendEmail = true;

    this.apiUsuariosService.sendEmailValidationByEmail(this.email).subscribe(
      () => {
        this.startTimer();
      },
      () => (this.disableResendEmail = false)
    );
  }

  openTicket() {
    this.clearStorage();
    const params = { state: { email: this.email } };
    this.navController.navigateForward(['/tickets/create'], params);
  }

  clearStorage() {
    this.storage.remove('email');
  }
}
