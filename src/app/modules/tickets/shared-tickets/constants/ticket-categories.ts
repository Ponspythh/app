export const TICKET_CATEGORIES = [
  { name: 'Mi cuenta/Registro', value: 'tickets.categories.my_account' },
  { name: 'Vincular mi cuenta de Binance', value: 'tickets.categories.link_binance_account' },
  { name: 'Comprar criptomonedas', value: 'tickets.categories.buy_crypto' },
  { name: 'Estrategias de inversión', value: 'tickets.categories.investment_strategies' },
  { name: 'Seguridad de mi capital', value: 'tickets.categories.capital_security' },
  { name: 'Wallet', value: 'tickets.categories.wallet' },
  { name: 'NFT', value: 'tickets.categories.nft' },
  { name: 'Otros', value: 'tickets.categories.others' },
  { name: 'Referidos', value: 'tickets.categories.referrals'},
  { name: 'Transacciones', value: 'tickets.categories.transactions'}
];
