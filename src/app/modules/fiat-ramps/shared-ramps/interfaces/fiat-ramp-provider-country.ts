export interface FiatRampProviderCountry {
  name: string;
  value: string;
  fiatCode?: string;
  isoCodeAlpha3: string;
  directaCode?: string;
  isoCurrencyCodeDirecta?: string;
}
