export const rawProviderCountriesData = [
  { name: 'Honduras', isoCodeAlpha3: 'HND', value: 'fiat_ramps.countries_list.honduras' },
  { name: 'Costa Rica', isoCodeAlpha3: 'CRI', value: 'fiat_ramps.countries_list.costa_rica', fiatCode: 'crc' },
  {
    name: 'Colombia',
    isoCodeAlpha3: 'COL',
    value: 'fiat_ramps.countries_list.colombia',
    fiatCode: 'cop',
    directaCode: 'CO',
  },
  {
    name: 'Argentina',
    isoCodeAlpha3: 'ARS',
    value: 'fiat_ramps.countries_list.argentina',
    fiatCode: 'ars',
    directaCode: 'AR',
  },
  {
    name: 'Ecuador',
    isoCodeAlpha3: 'ECU',
    value: 'fiat_ramps.countries_list.ecuador',
    directaCode: 'EC',
    isoCurrencyCodeDirecta: 'USD',
  },
];
