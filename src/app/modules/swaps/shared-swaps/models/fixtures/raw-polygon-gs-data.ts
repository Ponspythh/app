import { RawPolygonGS } from "../polygon-gas-price/polygon-gas-price";


export const rawPolygonGasStation: RawPolygonGS = {"safeLow":{"maxPriorityFee":33.664479934266666,"maxFee":35.494281265266665},"standard":{"maxPriorityFee":34.10264011906666,"maxFee":35.93244145006666},"fast":{"maxPriorityFee":35.6459197674,"maxFee":37.4757210984},"estimatedBaseFee":1.829801331,"blockTime":2,"blockNumber":30375091};
