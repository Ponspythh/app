export interface MenuItem {
  name: string;
  text: string;
  route: string;
  buttonName?: string;
  type: 'link';
}
