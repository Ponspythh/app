import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule, NavController } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { HideTextPipe } from 'src/app/shared/pipes/hide-text/hide-text.pipe';
import { LocalStorageService } from 'src/app/shared/services/local-storage/local-storage.service';
import { FakeNavController } from 'src/testing/fakes/nav-controller.fake.spec';
import { FakeTrackClickDirective } from 'src/testing/fakes/track-click-directive.fake.spec';
import { TrackClickDirectiveTestHelper } from 'src/testing/track-click-directive-test.spec';
import { WalletTotalBalanceCardComponent } from './wallet-total-balance-card.component';

describe('WalletTotalBalanceCardComponent', () => {
  let component: WalletTotalBalanceCardComponent;
  let fixture: ComponentFixture<WalletTotalBalanceCardComponent>;
  let trackClickDirectiveHelper: TrackClickDirectiveTestHelper<WalletTotalBalanceCardComponent>;
  let fakeNavController: FakeNavController;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let localStorageService: LocalStorageService;
  let localStorageServiceSpy: jasmine.SpyObj<LocalStorageService>;

  beforeEach(
    waitForAsync(() => {
      fakeNavController = new FakeNavController();
      navControllerSpy = fakeNavController.createSpy();
      localStorageServiceSpy = jasmine.createSpyObj(
        'LocalStorageService',
        {
          toggleHideFunds: undefined,
        },
        { hideFunds: of(false) }
      );
      localStorageServiceSpy.toggleHideFunds.and.callThrough();
      TestBed.configureTestingModule({
        declarations: [WalletTotalBalanceCardComponent, HideTextPipe, FakeTrackClickDirective],
        imports: [TranslateModule.forRoot(), IonicModule.forRoot(), HttpClientTestingModule],
        providers: [
          { provide: NavController, useValue: navControllerSpy },
          { provide: LocalStorageService, useValue: localStorageServiceSpy },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();

      fixture = TestBed.createComponent(WalletTotalBalanceCardComponent);
      component = fixture.componentInstance;
      trackClickDirectiveHelper = new TrackClickDirectiveTestHelper(fixture);
      localStorageService = TestBed.inject(LocalStorageService);
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should track ux_go_to_wallet on trackService when Go To Wallet is clicked and wallet exists', () => {
    component.walletExist = true;
    component.ngOnChanges();
    const el = trackClickDirectiveHelper.getByElementByName('div', 'Go To Wallet');
    const directive = trackClickDirectiveHelper.getDirective(el);
    const spy = spyOn(directive, 'clickEvent');
    el.nativeElement.click();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(directive.dataToTrack.eventLabel).toEqual('ux_go_to_wallet')
  });

  it('should track ux_create_go_to_home_wallet on trackService when Go To Wallet is clicked and wallet does not exist', () => {
    component.walletExist = false;
    component.ngOnChanges();
    const el = trackClickDirectiveHelper.getByElementByName('div', 'Go To Wallet');
    const directive = trackClickDirectiveHelper.getDirective(el);
    const spy = spyOn(directive, 'clickEvent');
    el.nativeElement.click();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(directive.dataToTrack.eventLabel).toEqual('ux_create_go_to_home_wallet')
  });

  it('should navigate to home wallet when Go To Wallet is clicked', () => {
    fixture.debugElement.query(By.css('div[name="Go To Wallet"]')).nativeElement.click();
    expect(navControllerSpy.navigateForward).toHaveBeenCalledWith(['tabs/wallets']);
  });

  it('should render balance card when wallet exist', async () => {
    component.walletExist = true;
    component.hideFundText = false;
    fixture.detectChanges();
    const divEl = fixture.debugElement.query(By.css('div.wbc__content_balance__body__balance'));
    expect(divEl.nativeElement.innerHTML).toContain('0.00');
  });

  it('should show title and subtitle when wallet not exist', async () => {
    component.walletExist = false;
    component.ngOnInit();
    await fixture.whenStable();
    await fixture.whenRenderingDone();
    fixture.detectChanges();
    const titleEl = fixture.debugElement.query(By.css('div.wbc__content__body__title'));
    const subtitleEl = fixture.debugElement.query(By.css('div.wbc__content__body__subtitle'));
    expect(titleEl.nativeElement.innerHTML).toContain('home.home_page.want_my_wallet.title');
    expect(subtitleEl.nativeElement.innerHTML).toContain('home.home_page.want_my_wallet.subtitle');
  });


  it('should render eye component', async () => {
    component.walletExist = true;
    fixture.detectChanges();
    const componentEl = fixture.debugElement.query(By.css('app-eye'));
    expect(componentEl).toBeTruthy();
  });

});
